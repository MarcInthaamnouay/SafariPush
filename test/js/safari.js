document.body.onload = function() {
    // Ensure that the user can receive Safari Push Notifications.
    if ('safari' in window && 'pushNotification' in window.safari) {
        var permissionData = window.safari.pushNotification.permission('web.marcintha.fr');
        checkRemotePermission(permissionData);
    }
};
 
var checkRemotePermission = function (permissionData) {
    console.log(permissionData);
    if (permissionData.permission === 'default') {
        console.log('default');
        // This is a new web service URL and its validity is unknown.
        window.safari.pushNotification.requestPermission(
            'https://berseck.fbdev.fr/push', // The web service URL.
            'web.marcintha.fr',     // The Website Push ID.
            {}, // Data that you choose to send to your server to help you identify the user.
            checkRemotePermission         // The callback function.
        );
    }
    else if (permissionData.permission === 'denied') {
        console.log(permissionData);
        // The user said no.
    }
    else if (permissionData.permission === 'granted') {
        console.log(permissionData);
        // The web service URL is a valid push provider, and the user said yes.
        // permissionData.deviceToken is now available to use.
    }
};
