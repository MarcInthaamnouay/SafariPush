# Safari push 

This projet is a POC of the safari push notification using PHP and Slim 

# Requirements 

Install the dependencies using the command composer install

# Change to do on the project to test it by youself

Please change the following files with the your following settings 

## Script.js

```javascript
window.safari.pushNotification.requestPermission(
        'yourWs', // The web service URL.
        'the reverse dns',     // The Website Push ID.
        {}, // Data that you choose to send to your server to help you identify the user.
        checkRemotePermission         // The callback function.
);
```

## MakePackage.php

```php

$this->cert_path = 'path of the p12 cert';
$this->intercert_path = 'path of the intermediary certificate';
        
$data = array('websiteName' => 'Accengage safari test',
            'websitePushID'   => 'the reverse dns ',
            'allowedDomains'  => [list of allowed domain],
            'urlFormatString' => 'url when the user click on the notification',
            'authenticationToken' => '19f8d7a6e9fb8a7f6d9330dabe',
            'webServiceURL'   => 'the webservice url');


```

## SendPush.php

```php
protected $cert = 'path of the .pem file (the pem generate from the p12)';
```

