<?php

require 'vendor/autoload.php';
require_once './src/controller/packageController.php';
require_once './src/services/makePackage.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// @TODO replace by config.json define the API version
$API_VERSION = 'v1';

$app = new Slim\App([
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,
    ]
]);


$container = $app->getContainer();
// Twig 
$container['view'] = function($container){
    $view = new \Slim\Views\Twig('./src/views/');
    
    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));

    return $view;
};
// Monolog
$container['logger'] = function($container){
    $log = new Logger('name');
    $log->pushHandler(new StreamHandler('./log/accengage.log', Logger::WARNING));

    return $log;
};

$app->get('/', function($request, $response, $args){
    return $this->view->render($response, 'index.twig');
});

$app->post('/foo', function($request, $response) {
    return $response->withJson(array("hello" => "hello"));
});

$app->post('/'.$API_VERSION.'/pushPackages/{webpushid}', function($request, $response){
    $config = json_decode(file_get_contents('./config.json'), true);
    $package = PackageController::buildPackage($config['package_suffix'], $config['package_path']);

    $this->logger->warning(json_encode($request->getParsedBody()));

    if($package){
        $fh = fopen($package, 'rb');
        $stream = new \Slim\Http\Stream($fh);
        return $response->withHeader('Content-type', 'application/zip')
                        ->withBody($stream);

        header("Content-Type: application/zip");
        header("Content-Disposition: attachment; filename=accengagesafaritest.pushpackage.zip");
        header("Content-Length: " . filesize($package));

        $this->logger->warning('call');

         readfile($package);
    } else {
        $this->logger->warning('error package');
        return $response->withJson(array('status' => 'error '.$package));
    }
});

$app->delete('/'.$API_VERSION.'/devices/{deviceToken}/registrations/{websitepushid}', function($request, $response ,$args){

});

$app->post('/'.$API_VERSION.'/log', function($request, $response){
    $logs = $request->getParsedBody();
    $this->logger->warning(json_encode($request->getParsedBody()));
});

$app->post('/'.$API_VERSION.'/sendPush', function($request, $response){ 
    PackageController::sendPush($request);
});

$app->run();