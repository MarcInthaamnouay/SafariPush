<?php

require_once './src/services/sendPush.php';

Class PackageController{

    public static function buildPackage($package_suffix, $package_path){
        $package = new makePackage($package_suffix, $package_path);

        try{
            $package->buildWebsiteJson();
            $package->copy_raw_push_package_files();
            $package->createManifest();
            $package->create_signature();
            $zip = $package->package_raw_data();

            return $zip;
        } catch (Exception $e){
            var_dump($e->getMessage());
        }
    }

    public static function sendPush($request){
        $title = $request->getParsedBody()['title'];
        $body = $request->getParsedBody()['body'];
        $action = $request->getParsedBody()['action'];
        $urlArgs = $request->getParsedBody()['args'];
        $token = $request->getParsedBody()['token'];

        $sender = new SendPush($title, $body, $action, $urlArgs, $token);
        $sender->send();
    }
}