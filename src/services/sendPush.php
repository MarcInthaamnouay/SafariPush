<?php

use Sly\NotificationPusher\PushManager,
    Sly\NotificationPusher\Adapter\Apns as ApnsAdapter,
    Sly\NotificationPusher\Collection\DeviceCollection,
    Sly\NotificationPusher\Model\Device,
    Sly\NotificationPusher\Model\Message,
    Sly\NotificationPusher\Model\Push
;

Class SendPush{

    protected $title;
    protected $body;
    protected $action;
    protected $urlArgs;
    protected $cert = './cert/cert.pem';
    protected $adapter;
    protected $manager;

    private $apnHosts = 'gateway.push.apple.com';
    private $apnPorts = 2195;
    private $token;
    private $pass;

    function __construct(){
        $this->title = "e";
        $this->body = "dd";
        $this->action = "";
        $this->urlArgs = "";
        $this->token = "7DC8C1291E281EAC10886971873CDF28718B83A6064EA57850AF01663B9FAA35";
        $this->pass = 'Marc0510';
        // Instantiate a manager
        // we only have the PROD Certificate
        $this->manager = new PushManager(PushManager::ENVIRONMENT_PROD);
    }

    /**
     *  Prepare
     *      Prepare the request toward the APNS
     *  @return Array $devices
     */
    protected function prepare(){
        $this->adapter = new ApnsAdapter(array(
            'certificate' => $this->cert,
            'passPhrase'  => $this->pass
        ));

        $devices = new DeviceCollection(array(
            new Device($this->token)
        ));

        return $devices;
    }

    /**
     *  Send
     *          Send a request toward the APNS
     */
    public function send(){
        // Prepare the APNS and the list of token to send the PUSH
        $devices = $this->prepare();
        $message = new Message('fuck you safari.', array(
            'badge' => 1,
            'sound' => 'example.aiff',

            'actionLocKey' => 'Action button title!',
            'locKey' => 'localized key',
            'locArgs' => array(
                'localized args',
                'localized args',
                'localized args'
            ),
            'launchImage' => 'image.jpg',
            'urlArgs' => array(),

            'custom' => array('custom data' => array(
                'we' => 'want', 'send to app'
            ))
        ));

        // build a PUSH to APNS
        $push = new Push($this->adapter, $devices, $message);
        $this->manager->add($push);
        $res = $this->manager->push();
        $feedback = $this->manager->getFeedback($this->adapter);
        var_dump($feedback);
        //print_r($res);
        //var_dump($res);
    }


}
