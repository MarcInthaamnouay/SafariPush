<?php

Class MakePackage{
    private $package_suffix;
    private $package_path;
    private $cert_path;

    // constant where we define the need files
    const FILES = [
        'icon_16x16.png',
        'icon_16x16@2x.png',
        'icon_32x32.png',
        'icon_32x32@2x.png',
        'icon_128x128.png',
        'icon_128x128@2x.png'
    ];

    function __construct($package_suffix, $package_path){
        $this->package_suffix = $package_suffix;
        $this->package_path = $package_path;
        $this->package_build_path = './build';
        $this->cert_path = './cert/certificats.p12';
        $this->intercert_path = './cert/AppleWWDRCA.pem';
    }

    /**
     *  Build Website Json
     *          Build the website json by taking the file from the json..
     *  @public
     *  @return boolean
     */
    public function buildWebsiteJson(){
        // @TODO if we use this we should make it dynamic though....
        $data = array('websiteName'     => 'Accengage safari test',
                      'websitePushID'   => 'web.com.accengage.pushweb',
                      'allowedDomains'  => ['https://51e6be31.eu.ngrok.io', 'https://ba9b0cdb.ngrok.io/'],
                      'urlFormatString' => 'https://www.google.fr',
                      'authenticationToken' => '19f8d7a6e9fb8a7f6d9330dabe',
                      'webServiceURL'   => 'https://ba9b0cdb.ngrok.io/SafariPush');

         $res = file_put_contents($this->package_build_path.'/website.json', json_encode($data, JSON_UNESCAPED_SLASHES));

         if(!$res)
            throw New Exception('cannot build the website.json');
    }

    /**
     *  Copy Raw Push Package Files
     *          Copy the package file to a temporary build folder
     *  @return boolean
     */
    public function copy_raw_push_package_files() {
        // first we need to check if the files is here 
        $contents = preg_grep('/^([^.])/', scandir($this->package_path.'/icon.iconset'));

        // check if each pictures is present otherwise thrown an error
        foreach($contents as $content){
            $isPresent = in_array($content, self::FILES);

            if (!$isPresent)
                throw New Exception('image is not present');
        }   

        mkdir($this->package_build_path.'/icon.iconset');
        foreach (self::FILES as $raw_file) {
            $res = copy($this->package_path."/icon.iconset/".$raw_file, $this->package_build_path."/icon.iconset/".$raw_file);

            if (!$res)
                throw New Exception('cannot copy files');
        }

        return true;
    }

    /**
     *  Create Manifest
     *          Create the manifest data
     *  @public
     *  @return boolean
     */
    public function createManifest(){
        $manifest_data = array();

        foreach(self::FILES as $file){
            $manifest_data['icon.iconset/'.$file] = hash("sha512", file_get_contents($this->package_path."/icon.iconset/".$file));
        }

        $manifest_data['website.json'] = hash("sha512", file_get_contents($this->package_build_path.'/website.json'));   
        $res = file_put_contents($this->package_build_path.'/manifest.json', json_encode((object)$manifest_data, JSON_UNESCAPED_SLASHES));   

        if (!$res)
            throw New Exception('error while creating the certificates');
    }


    /**
     *  Create Signature 
     *          Create a signature based credits to Apple.
     *  @private 
     */
    public function create_signature() {
        // Load the push notification certificate
        $pkcs12 = file_get_contents($this->cert_path);
        $certs = array();
        if(!openssl_pkcs12_read($pkcs12, $certs, 'accengage')) {
            throw new Exception('cannot read certificate');
            return;
        }

        $signature_path = $this->package_build_path.'/signature';

        // Sign the manifest.json file with the private key from the certificate
        $cert_data = openssl_x509_read($certs['cert']);
        $private_key = openssl_pkey_get_private($certs['pkey'], 'accengage');
        openssl_pkcs7_sign($this->package_build_path."/manifest.json", $signature_path, $cert_data, $private_key, array(), PKCS7_BINARY | PKCS7_DETACHED, $this->intercert_path);

        // Convert the signature from PEM to DER
        $signature_pem = file_get_contents($signature_path);
        $matches = array();
        if (!preg_match('~Content-Disposition:[^\n]+\s*?([A-Za-z0-9+=/\r\n]+)\s*?-----~', $signature_pem, $matches)) {
            return;
        }
        $signature_der = base64_decode($matches[1]);
        $res = file_put_contents($signature_path, $signature_der);

        if (!$res){
            var_dump('error creating signature');
            return 'error while creating signature';
        }
            

        $this->package_raw_data();
    }

    /**
     *  Package raw data
     *          Make the zip and return it to the front
     */
    public function package_raw_data() {
        $zip_path = 'accengagesafaritest.pushpackage.zip';

        // Package files as a zip file
        $zip = new ZipArchive();
        if (!$zip->open($zip_path, ZIPARCHIVE::CREATE)) {
            var_dump('could not create fuckign zip');
            error_log('Could not create ' . $zip_path);
            return;
        }

        $raw_files = array(
            'icon.iconset/icon_16x16.png',
            'icon.iconset/icon_16x16@2x.png',
            'icon.iconset/icon_32x32.png',
            'icon.iconset/icon_32x32@2x.png',
            'icon.iconset/icon_128x128.png',
            'icon.iconset/icon_128x128@2x.png',
            'website.json'
        );
        $raw_files[] = 'website.json';
        $raw_files[] = 'manifest.json';
        $raw_files[] = 'signature';
        
        foreach ($raw_files as $raw_file) {
            $zip->addFile($this->package_build_path."/".$raw_file, $raw_file);
        }

        $zip->close();
        return $zip_path;
    }
}