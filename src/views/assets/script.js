/**
 * -------- How can we interact with the WebSDK ---------- 
 * (!) Experimental idea
 * 
 *          (! Ask for Optinisation !)
 *              In our top controller we can have a safari Helper which'll act as a Controlle for the whole safari notifications
 *              It only need to do one thing. 
 *                          - Ask the user to be Optin 
 *                          - Forward if the user is Optin / Soft Optout / Hard Optout toward the backend 
 *                          - Using the snippet we can use a command let's say ACC.Safari({params...}) // Look at the section Optinisation case for the params
 *                          - The backend will response with a PACKAGE made by the back-end or the interface...
 *    
 *                          (Safari take in charge how we show the notifications)
 * 
 *          (! Optinisation case !)
 *              As the webSDK can be loaded it generate a deviceID 
 *                  WHEN the user become OPTIN we can FORWARD the deviceID + the deviceToken toward the back end 
 *                  with the partnerID 
 *                   
 *                  IF the user said NO to the Show Alert 
 *                  we can still make a request toward the backend an precise that for this deviceID the user is Hard Optout 
 * 
 *                  IF the user said YES to the Show Alert or Launch Landing On Click 
 *                  we can still make a request towardthe backend with the deviceID and tell it that the user is Hard Optout (if the user said no to the promp 
 *                  window it'll deny the optinization definitively without an update coming from the user)
 *          
 *          (! Package creation !)
 *                  IF we use NodeJS (so the back-end will have to do everything)
 *                      WE need to implement a CRUD with the same parameters as this PHP example. 
 *                      There's also a great Library for communicating with TLS to the APNS    
 *                  IF we use the PHP 
 *                      WE can use the POC done as an example...
 * 
 *                  (! Global IDEA !)
 *                  WHEN we create an Application the back-end or the interface will generate the required assets based on the image input on the interface
 *                  THIS assets will be store in a temporary place with a folder ID for each Application 
 *                  Then using the Domain saved (Domaine Enregistré) on the Interface, we can use it to build the Website.json need for Safari  
 *                  to communicate with apns 
 *                  The website JSON structure can be like this (based on the Apple guidelines and the information provided on the Interface)
 * 
 *                  website.json
 *                         -- websiteName : Nom (Interface)
 *                         -- websitePushId : a single reverse domain where our API will be (it's not necessary to be on the same domain)
 *                         -- allowedDomain : Domaine enregistré 
 *                         -- urlFormatString : URL par défault lors d'un click sur une notification 
 *                         -- webServiceUrl : the APIs
 * 
 *          (! Push creation !)
 *                  For a custom web adress we can use the placeholder like this 
 *                          -- the default value in the website.json will be 'http://%@/?%@'
 *                              where %@ are the datas in the url-args array. The first index can be Rich push URL the 
 *                              other arg can be the paramètres supplémentaires
 *                          -- we can still use the same templating 
 *                          -- when saving this will create a single String that will be send to the alert body's object 
 *                          -- Vignette ??? 
 *                                  (! Issue with the Vignette feature !)
 *                                   If we want to send a custom images we have to BUILD an other packages which is not suitable for Safari 
 *                                   See post : http://stackoverflow.com/questions/22359078/how-to-force-update-safari-push-package
 *                          -- Multilang support 
 *                                  The backend will have to handle the multilang for each token ....
 *                                  See doc : https://developer.apple.com/library/content/documentation/NetworkingInternet/Conceptual/NotificationProgrammingGuideForWebsites/PushNotifications/PushNotifications.html
 *                          -- feedback
 *                                  It looks like that we can't have any feedback                     
 *              
 *          (! Workflow !)
 *                  A user become optin 
 *                          --> When creating an application --> also create a push package
 *                          --> When asking to be optin for a website --> send the associate push package 
 *                          --> The safari push make forward data to the DB 
 *                          --> When sending a push, the back-end will use the deviceToken and check using the db if it's safari
 *                          --> If safari, use the apns to send data. 
 *                              Be aware that the templating and the custom params should be passed accordingly to Apple guidelines 
 *                              Multilanguage should be done by the back end (meaning that the push send to a deviceToken should be in the right lanugages)
 *                              Vignette --> Not possible without rebuilding the entire push package
 *                              Feedback --> Not possible for the moment we need to ask on stackoverflow or Apple dev forum
 *                              We can still have the subtrack if we use the show alert and the launch landing on click which will then ask the permission to be optin
 * 
 *          (! Issue !)
 *                  
 *                              
 *                  
 */



/**
 *  Safari Plugin
 *          A Plugin POC plugin that need to be integrated in the Web SDK
 *  @public 
 */
const safariPlugin = (function(){
    let isSafari = false;
    // bind a button

    /**
     *  Check Remote Permission 
     *  @public 
     *  @param {Object} permissionData
     */
    const checkRemotePermission = (permissionData) => {
        if (permissionData.permission === 'default')
            // This is a new web service URL and its validity is unknown.
            window.safari.pushNotification.requestPermission(
                'https://ba9b0cdb.ngrok.io/SafariPush', // The web service URL.
                'web.com.accengage.pushweb',     // The Website Push ID.
                {partnerID : 'marcdev'}, // Data that you choose to send to your server to help you identify the user.
                checkRemotePermission         // The callback function.
            );
        else if (permissionData.permission === 'denied')
            document.getElementById('res').innerHTML = 'denied';
        else if (permissionData.permission === 'granted')
            // we can make a request toward the backend and pass data using the deviceID + deviceToken which'll be the TOKEN 
            document.getElementById('res').innerHTML = 'granted';
    }

    /**
     *  Request Perm 
     *          Request the permission 
     *  @public 
     */
    const requestPerm = () => {
        console.log(isSafari);
        if(!isSafari)
            return;
        
        let permission = window.safari.pushNotification.permission('web.com.accengage.pushweb');
        checkRemotePermission(permission);
    };

    document.getElementById('getPermission').addEventListener('click', requestPerm);

    document.addEventListener('DOMContentLoaded', () => {
        if ('safari' in window && 'pushNotification' in window.safari) 
            isSafari = true;
    });
})();


// test slave domain
// safari push handler
